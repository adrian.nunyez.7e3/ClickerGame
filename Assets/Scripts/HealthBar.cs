﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

	[SerializeField] private Slider slider;
	[SerializeField] private Gradient gradient;
	[SerializeField] private Image fill;

	[SerializeField] private float maxHealth = 100;
	[SerializeField] float currentHealth;

	private HealthBar healthBar;

	void Start()
	{

		healthBar = gameObject.GetComponent<HealthBar>();

		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);

	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			DownBar(20);
		}
		if (Input.GetKey("2"))
		{
			UpBar(1);
		}
	}

    private void FixedUpdate()
    {
        
    }

    public void SetMaxHealth(float health)
	{
		slider.maxValue = health;
		slider.value = health;

		fill.color = gradient.Evaluate(1f);
	}

    public void SetHealth(float health)
	{
		slider.value = health;

		fill.color = gradient.Evaluate(slider.normalizedValue);
	}

	void DownBar(float down)
	{
        if (!(currentHealth <= 0))
        {
			currentHealth -= down;
			healthBar.SetHealth(currentHealth);
		}
	}

	void UpBar(float up)
	{
        if (currentHealth < maxHealth)
        {
			currentHealth += up;
			healthBar.SetHealth(currentHealth);
		}

	}

}
